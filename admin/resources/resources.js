"use strict";

angular.module("adminceylon")

.factory("Bddconcept", ["$resource", function($resource) {
    return $resource("http://localhost:9000/bdd/concept")
}])

.factory("Bddaccueil", ["$resource", function($resource) {
    return $resource("http://localhost:9000/bdd/accueil")
}])