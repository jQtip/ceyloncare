"use strict";
angular.module("adminceylon").component("concept", {
    templateUrl: "components/concept/concept.html",
    controller: concept
})

function concept($scope, $resource, $stateParams, Bddconcept, Bddaccueil, $http) {
    $scope.autoExpand = function(e) {
        var element = typeof e === 'object' ? e.target : document.getElementById(e);
        var scrollHeight = element.scrollHeight - 60; // replace 60 by the sum of padding-top and padding-bottom
        element.style.height = scrollHeight + "px";
    };

    function expand() {
        $scope.autoExpand('textarea1');
        $scope.autoExpand('textarea2');
        $scope.autoExpand('textarea3');
        $scope.autoExpand('textarea4');
        $scope.autoExpand('textarea5');
    }

    // }
    // let newconcept = {
    //     fr: {
    //         critere1: {
    //             title: "",
    //             paragraphe: ""
    //         },
    //         critere2: {
    //             title: "",
    //             paragraphe: ""
    //         },
    //         critere3: {
    //             title: "",
    //             paragraphe: ""
    //         },
    //         critere4: {
    //             title: "",
    //             paragraphe: ""
    //         }
    //     },
    //     en: {
    //         critere1: {
    //             title: "",
    //             paragraphe: ""
    //         },
    //         critere2: {
    //             title: "",
    //             paragraphe: ""
    //         },
    //         critere3: {
    //             title: "",
    //             paragraphe: ""
    //         },
    //         critere4: {
    //             title: "",
    //             paragraphe: ""
    //         }
    //     }
    // }
    let newconcept = ""

    Bddconcept.get().$promise.then((data) => {
        newconcept = data.bdd
        this.newconcept = newconcept
    })

    this.save = save

    function save() {
        $http.put('/bdd/concept', newconcept);
        alert("Les données ont été sauvegardées")
    }
}