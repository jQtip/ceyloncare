"use strict"

var passport = require('passport');
var mongoose = require('mongoose');
var bcrypt = require('bcryptjs')
var Schema = mongoose.Schema;

/**
 * Build the user schema
 */
let UserSchema = new mongoose.Schema({
    username: {
    type: String,
    required: true
  },    
    password: {
    type: String,
    required: true,
    select: false
  }
});

module.exports = mongoose.model('Account', Account);