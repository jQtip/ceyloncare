'use strict'

//*************************************//
//                                     //
//            NOS MODULES              //
//                                     //
//*************************************//

const
    express = require("express"), //express
    app = express(), //on renomme express app
    bodyParser = require("body-parser"), // on parse la data 
    cookieParser = require("cookie-parser"), // cookie
    bcrypt = require("bcrypt-nodejs"), // on crypte 
    flash = require("connect-flash"), // on recoit nos donnnées flash 
    multer = require('multer'), // pour upload les données
    session = require("express-session"),
    eValidator = require("express-validator"),
    mongoose = require("mongoose"),
    morgan = require("morgan"),
    passport = require("passport"),
    localStrategy = require("passport-local").Strategy,
    path = require("path"),
    port = process.env.port || 9000,
    coloreus = require("colors"),
    storage = multer.diskStorage({ // on défini le dossier ou l'on va storer les images
        destination: function(req, file, callback) {
            callback(null, '../public/assets/images');
        },
        filename: function(req, file, callback) {
            callback(null, file.fieldname);
            console.log(file)
        }
    });

///

app

    .all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
})

//*************************************//
//                                     //
//            NOS MIDDLEWARES          //
//                                     //
//*************************************//


app.use(express.static("../public/")) // on definit le dossier statique (la route principale)

app.use(bodyParser.urlencoded({ extended: true })) // on recoit nos données + sécu

app.use(bodyParser.json()) //on transforme ce qu'on recoit en JSON 

app.use('/auth', express.static('auth')) // en initie la route auth et on donne la possibilité d'y accéder

app.use(session({
    secret: 'DevlankaInDaPlace', // Mot secret pour créer le hash
    resave: false, // Si true, la session va être sauvegardé dans le stockage
    saveUninitialized: false // Si true, La session non initialisée va être sauvegardé dans le stockage
}))

app.all("/admin/*", requireLogin, function(req, res, next) {
    next(); // Si le middleware requireLogin nous permet d'arriver ici,
    // on se déplace au gestionnaire de routes suivant
})

.use('/admin', express.static('../admin'));


////        On se connecte a mongoose et on crée la collection (qui contiendra nos document)


//*************************************//
//                                     //
//            NOS ROUTES               //
//                                     //
//*************************************//

app.get('/auth', function(req, res) {
    // console.log(__dirname);
    res.sendFile(path.join(__dirname + '/auth/index.html'));
})

.post('/auth', function(req, res) {

    if (req.body.usr && req.body.psw) {
        console.log("pair of values received!");
        if (req.body.usr === 'Ceylon' && req.body.psw === 'Care') {
            req.session.authenticated = true;
        }
    } else {
        console.log("User name and/or password undefined or incorrect");
    }

    res.redirect('/redirects');
})

.get('/redirects', function(req, res) {
    if (req.session.authenticated) {
        console.log('You are in!');
        res.redirect('/admin');
    } else {
        res.end("<script> alert('Votre identifiant ou mot de passe sont incorrects') </script>");
    }
})

.get('/logout', function(req, res) {
    req.session.destroy(function(err) {
        // console.log(err);
        res.redirect('/auth');
        console.log("You've been logout");
    });
});


// The following "requireLogin" middleware protects a particular
// route(s), in our case /admin/*, from being accessed
function requireLogin(req, res, next) {
    if (req.session.authenticated) {
        next(); // allow the next route to run
    } else {
        // require the user to log in
        res.redirect("/auth"); // or render a form, etc.
    }
}



let upload = multer({ storage: storage }).any(); // configuration du plugin multer


mongoose.connect('mongodb://localhost/ceylonBdd')

////        On définit nos Schema Mongoose pour Mongo db 

app.use("/admin", express.static("../admin/"))

//// app.post qui sert a multer pour upload les images 

.post("/", function(req, res, next) {
    upload(req, res, function(err) {
        if (err) {
            return res.end("<script> alert('Erreur') </script>");
        }
        // res.send("<script> alert('La sauvegarde a été effectuée'); window.history.back()</script>");
        console.log("ok")
        console.log(req.body)
    });
});

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////            On fait l'API (le GET PUT DELETE ... ) LE CRUD 



////        On déclare notre mschema mongoose pour /concept

const staticInnerCeylon = mongoose.Schema({
    _id: String,
    fr: {
        critere1: {
            title: String,
            paragraphe: String
        },
        critere2: {
            title: String,
            paragraphe: String
        },
        critere3: {
            title: String,
            paragraphe: String
        },
        critere4: {
            title: String,
            paragraphe: String
        },
        critere5: {
            title: String,
            paragraphe: String
        }
    },
    en: {
        critere1: {
            title: String,
            paragraphe: String
        },
        critere2: {
            title: String,
            paragraphe: String
        },
        critere3: {
            title: String,
            paragraphe: String
        },
        critere4: {
            title: String,
            paragraphe: String
        },
        critere5: {
            title: String,
            paragraphe: String
        }
    }
})

let ceylonCrud = mongoose.model("ceylonCrud", staticInnerCeylon);

let router = express.Router();

////            On fait la route 

router

////            Sur / quand on demande la base de données
    .route("/")
    .get(function(req, res) {
        ceylonCrud.find(function(err, bdd) {
            if (err) {
                res.send(err);
            }
            res.json({ bdd })
        })
    })

////            Sur le / quand  on poste notre nouveau bgground

.post(function(req, res) {
    let newbdd = new ceylonCrud();
    console.log("request", req.body)
    newbdd._id = req.body._id
    newbdd.fr.critere1.title = req.body.fr.critere1.title
    newbdd.fr.critere1.paragraphe = req.body.fr.critere1.paragraphe
    newbdd.fr.critere2.title = req.body.fr.critere2.title
    newbdd.fr.critere2.paragraphe = req.body.fr.critere2.paragraphe
    newbdd.fr.critere3.title = req.body.fr.critere3.title
    newbdd.fr.critere3.paragraphe = req.body.fr.critere3.paragraphe
    newbdd.fr.critere4.title = req.body.fr.critere4.title
    newbdd.fr.critere4.paragraphe = req.body.fr.critere4.paragraphe
    newbdd.fr.critere5.title = req.body.fr.critere4.title
    newbdd.fr.critere5.paragraphe = req.body.fr.critere4.paragraphe
    newbdd.en.critere1.title = req.body.en.critere1.title
    newbdd.en.critere1.paragraphe = req.body.en.critere1.paragraphe
    newbdd.en.critere2.title = req.body.en.critere2.title
    newbdd.en.critere2.paragraphe = req.body.en.critere2.paragraphe
    newbdd.en.critere3.title = req.body.en.critere3.title
    newbdd.en.critere3.paragraphe = req.body.en.critere3.paragraphe
    newbdd.en.critere4.title = req.body.en.critere4.title
    newbdd.en.critere4.paragraphe = req.body.en.critere4.paragraphe
    newbdd.en.critere5.title = req.body.en.critere5.title
    newbdd.en.critere5.paragraphe = req.body.en.critere5.paragraphe
    newbdd.save(function(err) { // pour sauvegarder dans la bdd
        if (err) {
            res.send(err)
        }
        res.send({ message: "Les données ont été sauvegardées" });

    })
});

////            Sur le / quand  on poste notre nouveau bgground on modifie également tout les inner des parties critere

router
////            Sur /:contact_id on prend l'id du contact


    .route("/:ceylonCrud_id")
    .get(function(req, res) {
        ceylonCrud.findOne({ _id: req.params.ceylonCrud_id }, function(err, bdd) {
            if (err) {
                res.send(err);
            }
            res.json({ bdd });
        })
    })

////             On modifie le contact 


.put(function(req, res) {
    ceylonCrud.findOne({ _id: req.params.ceylonCrud_id }, function(err, bdd) {
        if (err) {
            res.send(err);
        }
        console.log(bdd)
        bdd.fr.critere1.title = req.body.fr.critere1.title
        bdd.fr.critere1.paragraphe = req.body.fr.critere1.paragraphe
        bdd.fr.critere2.title = req.body.fr.critere2.title
        bdd.fr.critere2.paragraphe = req.body.fr.critere2.paragraphe
        bdd.fr.critere3.title = req.body.fr.critere3.title
        bdd.fr.critere3.paragraphe = req.body.fr.critere3.paragraphe
        bdd.fr.critere4.title = req.body.fr.critere4.title
        bdd.fr.critere4.paragraphe = req.body.fr.critere4.paragraphe
        bdd.fr.critere5.title = req.body.fr.critere5.title
        bdd.fr.critere5.paragraphe = req.body.fr.critere5.paragraphe
        bdd.en.critere1.title = req.body.en.critere1.title
        bdd.en.critere1.paragraphe = req.body.en.critere1.paragraphe
        bdd.en.critere2.title = req.body.en.critere2.title
        bdd.en.critere2.paragraphe = req.body.en.critere2.paragraphe
        bdd.en.critere3.title = req.body.en.critere3.title
        bdd.en.critere3.paragraphe = req.body.en.critere3.paragraphe
        bdd.en.critere4.title = req.body.en.critere4.title
        bdd.en.critere4.paragraphe = req.body.en.critere4.paragraphe
        bdd.en.critere5.title = req.body.en.critere5.title
        bdd.en.critere5.paragraphe = req.body.en.critere5.paragraphe
        bdd.save(function(err) {
            if (err) {
                res.send(err)
            }
            res.send({ message: "Les données ont été mises à jour" })
        })
    })
})

////            On delete le contact 


.delete(function(req, res) {
    ceylonCrud.remove({ _id: req.params.ceylonCrud_id }, function(err) {
        if (err) {
            res.send(err)
        }
        res.send({ message: "Les données ont été supprimées" })
    })
});
app.use("/bdd", router);



////            On run le serveur sur le port (const port = 9000)

app.listen(port,
    function() {
        console.log("La donzomobile démarre http://localhost:".rainbow + port)
    })