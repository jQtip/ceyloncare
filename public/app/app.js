"use strict";

angular.module("ceyloncare", [
    // les dépendances externes
    "ui.router", "ngResource"
])

.config(function($stateProvider, $urlRouterProvider) {
    var states = [{
            name: "accueil",
            url: "/",
            component: "accueil"
        }, {
            name: "concept",
            url: '/concept',
            component: "concept",
        }, {
            name: "contact",
            url: "/contact",
            component: "contact"
        }, {
            name: "equipe",
            url: "/equipe",
            component: "equipe"
        }, {
            name: "menu",
            url: "/menu",
            component: "menu"
        },
        // {
        //     name: "admin",
        //     url: "/admin",
        //     component: "admin"
        // }

    ];
    $urlRouterProvider.otherwise("/");
    states.forEach(function(state) {
        $stateProvider.state(state);
    })
})

;