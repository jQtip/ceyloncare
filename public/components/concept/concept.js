"use strict";
angular.module("ceyloncare").component("concept", {
    templateUrl: "components/concept/concept.html",
    controller: concept
})

function concept($scope, $resource, $stateParams, Bddconcept) {

    let newconcept = ""

    Bddconcept.get().$promise.then((data) => {
        newconcept = data.bdd
        console.log(newconcept)
        $scope.newconcept = newconcept
    })


}