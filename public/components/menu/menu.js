"use strict";
angular.module("ceyloncare").component("menu", {
    templateUrl: "components/menu/menu.html",
    controller: menu
})

function menu($scope, $resource, $stateParams) {
    this.states = [{

            name: "home",
            displayName: "Accueil"
        },
        {
            name: "concept",
            displayName: "Concept"
        },
        {
            name: "equipe",
            displayName: "Équipe"
        },
        {
            name: "contact",
            displayName: "Contact"
        },
        {
            name: "inscription",
            displayName: "Inscription"
        }
    ]

    $(".dropdown-button").dropdown();

}