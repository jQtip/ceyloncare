"use strict";

angular.module("ceyloncare")

.factory("Bddconcept", ["$resource", function($resource) {
    return $resource("http://localhost:9000/bdd/concept")
}])