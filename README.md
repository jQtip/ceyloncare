CEYLONCARE

Le but du projet et de créer un site qui propose des circuits touristiques personnalisés, de manière équitable et responsable. Pour la réalisation de ce projet, nous avons utilisé le MEAN stack c’est à dire MongoDB, ExpressJS,AngularJS et NodeJS.

Ressources supplémentaires:
⁃	FullCalendar
⁃	Nodemailer
⁃	Express
⁃	BodyParser
⁃	Materialize
⁃	Passport
⁃	Momentjs
⁃	Bcrypt
⁃	Https
⁃	Morgan
⁃	Nodemon
⁃	Mongoose
⁃	Mongodb
⁃	Ui.router
⁃	Multer
⁃	Jquery
⁃	Colors
||| INSTALLATION |||

Ouvrir un terminal puis entrer les commandes suivantes:

$ curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash - sudo apt-get install -y nodejs $ sudo apt-get install mongodb-server $ sudo service mongodb start

// NB: le service mongodb doit être actif sur la machine

$ git clone https://jQtip@bitbucket.org/jQtip/ceyloncare.git $ cd bdx0317-CeylonCare/server $ npm run migrate $ npm run make

||| FRONT-END |||

Plusieurs sections: •	Accueil •	Concept •	Equipe •	Contact •	Circuits

La partie Circuits est composée de plusieurs étapes et utilise la technologie angular-switch: •	Choix du nombre de voyageurs •	Choix du mois de départ •	Choix du budget •	Choix du thème •	Affichage du circuit proposé sur une carte utilisant des fichiers KML prédéfinis

||| BACK-END |||

Plusieurs sections: •	Accueil •	Circuits •	Concept •	Equipe •	Contact •	Mot de passe •	Logout

NODEMAILER

Pour l'envoie de mail nous utilisons la librairie Nodemail. Voir la documentation : [Nodemail] (https://nodemailer.com/about/)

PASSPORT

Pour l'authentification nous utilisons la librairie Passport. Voir la documentation : [Passport] (http://passportjs.org/)

L’équipe Devlanka •	François Alexandre Colombani •	Carole Hermouet •	Julien Berreau •	Davy Donzo •	Olivier Taieb